#!C:\Program Files\PowerShell\7\pwsh.exe
#--------------------------------------------------------------
#
#    Launching the Msys2 in a fully configured way.
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#---------------------------------------------------------------------------------
param([string]$DRIVE        = "DRIVE",
      [string]$FOLDER       = "FOLDER",
	  [string]$VERSION      = "VERSION",
	  [string]$VENDOR       = "VENDOR",
	  [string]$BOARD        = "BOARD",
      [string]$CPU          = "CPU",
	  [string]$PRODUCT      = "PRODUCT",
	  [string]$SKETCH       = "SKETCH",
	  [string]$SKETCH_VER   = "SKETCH_VER",
	  [string]$ARCHITECTURE = "ARCHITECTURE",
	  [string]$PROTOCOL     = "PROTOCOL",
	  [string]$PROBE        = "PROBE" ) ;


#Read-Host -Prompt "Pausing:  Press any key to continue"

$BASE_PATH       = "${DRIVE}:\${FOLDER}"
$ARDUINO_PATH    = "$BASE_PATH\arduino-$VERSION"
$PORTABLE_PATH   = "$ARDUINO_PATH\portable"
$PREFS_PATH      = "$ARDUINO_PATH\portable"
$SKETCH_PATH     = "$PORTABLE_PATH\sketchbook\cmake\${VENDOR}\${CPU}"
$BUILD_PATH      = "${SKETCH_PATH}\${PRODUCT}-${SKETCH}-${SKETCH_VER}"
$SKETCH_NAME     = "${PRODUCT}-${SKETCH}-${SKETCH_VER}"
$USER            ="$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH    = "/home/$env:USER"
$SCRIPT_PATH     = "${DRIVE}:\bin\pwshell"
$OPENOCD_PATH    = "${DRIVE}:\${FOLDER}\openocd\bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"



#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Pausing:  Press any key to continue"

    echo "Line $(CurrentLine)   Calling                Launch_jtagit_prog()"
    Launch_jtagit_prog

}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:\"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)   Entering               echo_args"
    Write-Host "Line $(CurrentLine)   Arguments              ";
    Write-Host "Line $(CurrentLine)   Drive                = ${DRIVE}";
    Write-Host "Line $(CurrentLine)   FOLDER               = ${FOLDER}";
    Write-Host "Line $(CurrentLine)   Arduino Version      = ${VERSION}";
    Write-Host "Line $(CurrentLine)   Board Vendor         = ${VENDOR}";
    Write-Host "Line $(CurrentLine)   Board Name           = ${BOARD}";
    Write-Host "Line $(CurrentLine)   CPU                  = ${CPU}";
    Write-Host "Line $(CurrentLine)   PRODUCT              = ${PRODUCT}";
    Write-Host "Line $(CurrentLine)   Sketch Name          = ${SKETCH}";
    Write-Host "Line $(CurrentLine)   Sketch Version      = ${SKETCH_VER}";
    Write-Host "Line $(CurrentLine)  Architecture         = ${ARCHITECTURE}";
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL}";
    Write-Host "Line $(CurrentLine)  Debug Probe          = ${PROBE}";
    Write-Host "Line $(CurrentLine)  BASE_PATH            = $BASE_PATH";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = $ARDUINO_PATH";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH        = $PORTABLE_PATH";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = $PREFS_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = $SKETCH_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME          = $SKETCH_NAME";            
    Write-Host "Line $(CurrentLine)  BUILD_PATH           = $BUILD_PATH";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = $ARDUINO_PATH";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH         = $OPENOCD_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH               = $SKETCH";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}


#---------------------------------------------------------
# Launch launcf_jtagit_prog
#---------------------------------------------------------
function launch_jtagit_prog
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_jtagit_prog()"
    Write-Host "Line $(CurrentLine)  Executing             'cd ${SKETCH_PATH}\${SKETCH_NAME}\build' "
    cd ${SKETCH_PATH}\${SKETCH_NAME}\build
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
#   ls -al
#echo $PATH
#Read-Host -Prompt "Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Protocol             = ${PROTOCOL} "
	Write-Host "Line $(CurrentLine)  Executing              '& ${OPENOCD_PATH}\openocd.exe -f board/eicon/win/${PROBE}/${CPU}/${PRODUCT}-${SKETCH}-${SKETCH_VER}.ino-${PROTOCOL}.cfg' "
    & ${OPENOCD_PATH}\openocd.exe -f board/eicon/win/${PROBE}/${CPU}/${PRODUCT}-${SKETCH}-${SKETCH_VER}.ino-${PROTOCOL}.cfg

Read-Host -Prompt "Pausing:  Press any key to continue"

    cd $SCRIPT_PATH
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
    
    Write-Host "Line $(CurrentLine)  Leaving                launch_jtagit_prog"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
echo "Line $(CurrentLine)  Calling                main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

