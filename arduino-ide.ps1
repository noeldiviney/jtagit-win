#!C:\Program Files\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
param([string]$DRIVE            = "DRIVE",
      [string]$FOLDER           = "FOLDER",
	  [string]$ARDUINO_VERSION  = "ARDUINO_VERSION",
	  [string]$VENDOR           = "VENDOR",
	  [string]$BOARD            = "BOARD",
      [string]$CPU              = "CPU", 
	  [string]$SKETCH           = "SKETCH",
	  [string]$ARCHITECTURE     = "ARCHITECTURE",
	  [string]$PROTOCOL         = "PROTOCOL" ) ;
#Read-Host -Prompt "Pausing:  Press any key to continue"

$BASE_PATH         = "${DRIVE}:\${FOLDER}"
$ARDUINO_PATH      = "${BASE_PATH}\arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH     = "${ARDUINO_PATH}\portable"
$PREFS_PATH        = "${PORTABLE_PATH}"
$SKETCH_PATH       = "${PORTABLE_PATH}\sketchbook\arduino\${VENDOR}"
$SKETCH_NAME       = "${CPU}-${BOARD}-${SKETCH}"
$SKETCH_PREFS_PATH = "${SKETCH_PATH}\${SKETCH_NAME}\preferences"
$USER              = "$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH      = "/home/$env:USER"
$SCRIPT_PATH       = "${DRIVE}:\bin\pwshell"
$OPENOCD_PATH      = "${DRIVE}:\bin\openocd\bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"
#Read-Host -Prompt 'Input your server  name'

    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Returning from         echo_args"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"

    Write-Host "Line $(CurrentLine)   Copy                   ${SKETCH_PREFS_PATH}\${SKETCH_NAME}.txt"
    Write-Host "Line $(CurrentLine)   To                     $PREFS_PATH/preferences.txt"
    Copy-Item ${SKETCH_PREFS_PATH}\${SKETCH_NAME}.txt $PREFS_PATH\preferences.txt
#Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                Launch_Arduino_IDE"
    Launch_Arduino_IDE
#Read-Host -Prompt "Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing              $ARDUINO_PATH\arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH\arduino
    Write-Host "Line $(CurrentLine)  Leaving                Launch_Arduino_IDE"
}


#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering               echo_args";
    Write-Host "Line $(CurrentLine)  DRIVE                = ${DRIVE} ";
    Write-Host "Line $(CurrentLine)  FOLDER               = ${FOLDER} ";
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION      = ${ARDUINO_VERSION} ";
    Write-Host "Line $(CurrentLine)  VENDOR               = ${VENDOR} ";
	Write-Host "Line $(CurrentLine)  BOARD                = ${BOARD} ";
    Write-Host "Line $(CurrentLine)  CPU                  = ${CPU} ";
	Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH} ";
    Write-Host "Line $(CurrentLine)  ARCHITECTURE         = ${ARCHITECTURE} ";
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL} ";
    Write-Host "Line $(CurrentLine)  BASE_PATH            = ${BASE_PATH} ";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH} ";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = ${PREFS_PATH} ";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME          = ${SKETCH_NAME} ";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = ${SKETCH_PATH} ";            
    Write-Host "Line $(CurrentLine)  SKETCH_PREFS_PATH    = ${SKETCH_PREFS_PATH} ";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH} ";            
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH} ";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args ";
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                main()"
main
