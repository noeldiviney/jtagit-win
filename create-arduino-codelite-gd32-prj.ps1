#!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass###!/usr/bin/env pwsh
###  #!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------
#$OS_LC                         = ${OS}.ToLower()

#---------------------------------------------------------
# Global Parameters
#--------------------------------------------------------

param(
      [string]$PROJ_NAME       = "PROJ_NAME",
      [string]$CORE_PLATFORM   = "CORE_PLATFORM",
      [string]$BRK_LINE_NO     = "BRK_LINE_NO");

if ($IsLinux)
{
      $OS_UC         = "LIN"
      $OS_LC         = "lin"
      $OS_C          = "Lin"
      $DRIVE         = "/home/eicon"
      $SL            = "/"
}
elseif ($IsWindows)
{
      $OS_UC         = "WIN"
      $OS_LC         = "win"
      $OS_C          = "Win"
      $DRIVE         = "C:"
      $SL            = "\"
    }
$PROJECT_NAME                  = "${PROJ_NAME}"
$BREAK_LINE_NO                 = "${BRK_LINE_NO}"
$INSTALL_FOLDER                = "J-Tagit"
$WORKSPACE_FOLDER              = "CodeLite"
$PROJECT_FOLDER                = "${PROJECT_NAME}"
$CORE_FOLDER                   = "Core_"+"${CORE_PLATFORM}".ToUpper()
$CPU_NAME                      = $PROJECT_NAME.SubString(0, 8)
$PROJECT_GD32_RELEASE          = "1.0.0"
$PLATFORM_GD32_NAME            = "gd32"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "1.0.1"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$BOARD_VENDOR_JT               = "J-Tagit"
$BOARD_VENDOR_JT_LC            = "j-tagit"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$ARM_GCC_VERSION               = "10.1.0"
$TEENSY_GCC_VERSION            = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"
$BREAK_LINE_NO                 = "${BREAK_LINE_NO}"
$TEMPL_PROJ_NAME               = "F103CBT6-BluePill-HelloW"
$PROBE                         = "jtagit-2"
$CPU_TARGET                    = "gd32f30x.cfg"
$BUILD_SYS                     = "cmake"
$PROTOCOL                      = "swd"
$OPENOCD_GD32_CFG_FILENAME     = "${PROJECT_NAME}-${BUILD_SYS}-${PROTOCOL}.cfg"
$OPENOCD_CFG_FILENAME          = "${PROJECT_NAME}-${BUILD_SYS}-${PROTOCOL}.cfg"
##                                http://192.168.0.14/noeldiviney/F303ZIT6-GDDuino-HelloW/-/archive/1.0.0/F303ZIT6-GDDuino-HelloW-1.0.0.zip
$PROJECT_STM32_URI             = "http://192.168.0.14/noeldiviney/${PROJECT_NAME_ST}/-/archive/${PROJECT_STM32_RELEASE}/${PROJECT_NAME_ST}-${PROJECT_STM32_RELEASE}.zip"
$PROJECT_GD32_URI              = "http://192.168.0.14/noeldiviney/${PROJECT_NAME}/-/archive/${PROJECT_GD32_RELEASE}/${PROJECT_NAME_GD}-${PROJECT_GD32_RELEASE}.zip"
$VARIANTS_STM32_URI            = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}/-/archive/${PROJECT_STM32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}-${PROJECT_STM32_RELEASE}.zip"
$VARIANTS_GD32_URI             = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}/-/archive/${PROJECT_GD32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$CORE_STM32_ZIP_URI            = "https://github.com/stm32duino/Arduino_${CORE_STM32_NAME}/archive/refs/tags/${PROJECT_STM32_RELEASE}.zip"
$CORE_GD32_ZIP_URI             = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}/-/archive/${PROJECT_GD32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
##                                http://192.168.0.14/noeldiviney/F303ZIT6-GDDuino-HelloW/-/archive/1.0.0/F303ZIT6-GDDuino-HelloW-1.0.0.zip
##                                http://192.168.0.14/noeldiviney/j-tagit-gd32-core-lin/-/archive/1.0.0/j-tagit-gd32-core-lin-1.0.0.zip
##                                https://gitlab.com/noeldiviney/F103CBT6_BluePill_HelloW_Lin/-/archive/2.2.0/F103CBT6_BluePill_HelloW_Lin-2.2.0.zip
##                                https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip
$PROJECT_GD32_ZIP_NAME         = "${PROJECT_NAME}-${PROJECT_GD32_RELEASE}.zip"
$PROJECT_GD32_UNZIPPED_NAME    = "${PROJECT_NAME}-${PROJECT_GD32_RELEASE}"
$ARDUINO_CORE_GD32_ZIP_NAME    = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$ARDUINO_CORE_GD32_NAME        = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}"
$VARIANTS_GD32_ZIP_NAME        = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$VARIANTS_GD32_NAME            = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}"


$OPENOCD_TB_TYPE               = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX      = "${BOARD_VENDOR_JT_LC}-arduino-libraries"
$SKETCHBOOK_NAME_PREFIX        = "${BOARD_VENDOR_JT_LC}-sketchbook"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
$EICON_GCC_NAME                = "${BOARD_VENDOR_JT_LC}-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE           = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE               = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME       = "${BOARD_VENDOR_JT_LC}-openocd-cfg"

$APP_FOLDER                    = "App"
$APP_SRC_FILENAME              = "${PROJECT_NAME}.cpp"
$BUILD_FOLDER                  = "build"
$CMAKE_FOLDER                  = "CMake"
$CMAKE_LIB_FILENAME            = "ArduinoLib.cmake"
$CMAKE_LIB_MSG_FILENAME        = "ArduinoLibMsg.cmake"
$CMAKELISTS_MSG1_FILENAME      = "CMakeListsMsg1.cmake"
$CMAKELISTS_MSG2_FILENAME      = "CMakeListsMsg2.cmake"
$CMAKE_TOOLCHAIN_FILENAME      = "Toolchain.cmake"
$CMAKE_TOOLCHAIN_msg_FILENAME  = "ToolchainMsg.cmake"
$LICENCE_FILENAME              = "LICENSE"
$README_FILENAME               = "README.adoc"
$CMAKELISTS_FILENAME           = "CMakeLists.txt"

$BASE_PATH                     = "${DRIVE}"
$INSTALL_FOLDER_PATH           = "${BASE_PATH}${SL}${INSTALL_FOLDER}"
$WORKSPACE_FOLDER_PATH         = "${INSTALL_FOLDER_PATH}${SL}${WORKSPACE_FOLDER}"
$PROJECT_FOLDER_PATH           = "${WORKSPACE_FOLDER_PATH}${SL}${PROJECT_FOLDER}"
$APP_FOLDER_PATH               = "${PROJECT_FOLDER_PATH}${SL}${APP_FOLDER}"
$BUILD_FOLDER_PATH             = "${PROJECT_FOLDER_PATH}${SL}${BUILD_FOLDER}"
$CMAKE_FOLDER_PATH             = "${PROJECT_FOLDER_PATH}${SL}${CMAKE_FOLDER}"

$CORE_FOLDER_PATH              = "${INSTALL_FOLDER_PATH}/${CORE_FOLDER}"
$DLOAD_PATH                    = "${INSTALL_FOLDER_PATH}/dload" 
$WORKSPACE_PATH                = "${INSTALL_FOLDER_PATH}/${WORKSPACE_FOLDER}"
$BUILD_FOLDER_PATH             = "${WORKSPACE_PATH}/${PROJECT_NAME}/build"
$APP_PATH                      = "${WORKSPACE_PATH}/${PROJECT_NAME}/App"
$ARDUINO_PATH                  = "${INSTALL_FOLDER_PATH}/arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}/portable"
$HARDWARE_PATH_JT              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_JT}/hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/hardware"
$TOOLS_PATH_JT                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_JT}/tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/tools"
$OPENOCD_PATH                  = "${INSTALL_FOLDER_PATH}/openocd"
$OPENOCD_CFG_PATH              = "${OPENOCD_PATH}/scripts/board/${BOARD_VENDOR_JT}/${OS_LC}/j-tagit"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}/sketchbook/arduino"
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  http://192.168.0.14/noeldiviney/j-tagit-openocd-cfg/-/archive/0.1.0/j-tagit-openocd-cfg-0.1.0.tar.gz
##  http://192.168.0.14/noeldiviney/j-tagit-sketchbook/-/archive/0.1.0/j-tagit-sketchbook-0.1.0.zip
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "http://192.168.0.14/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI            = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-sketchbook/-/archive/${SKETCHBOOK_RELEASE}/${BOARD_VENDOR_JT_LC}-sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$GD32_INDEX                    = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_gd32_index.json,"
$STM32_INDEX                   = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_stm32_index.json,"
$KINETIS_INDEX                 = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "${BOARD_VENDOR_JT_LC}_${OS_LC}_arduino_libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${BOARD_VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                          = "$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH                  = "/home/$env:USER"
$SCRIPT_PATH                   = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE              = "${BOARD_VENDOR_JT_LC}-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files/Notepad++/notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)   Entering                        echo_args";
    Write-Host "Line $(CurrentLine)   OS_UC                         = ${OS_UC}";
    Write-Host "Line $(CurrentLine)   OS_LC                         = ${OS_LC}";
    Write-Host "Line $(CurrentLine)   OS_C                          = ${OS_C}";
    Write-Host "Line $(CurrentLine)   DRIVE                         = ${DRIVE}";
    Write-Host "Line $(CurrentLine)   SLASH                         = ${SL}";
    Write-Host "Line $(CurrentLine)   BREAK_LINE_NO                 = ${BREAK_LINE_NO}";
    Write-Host "Line $(CurrentLine)   INSTALL_FOLDER                = ${INSTALL_FOLDER}";	
    Write-Host "Line $(CurrentLine)   WORKSPACE_FOLDER              = ${WORKSPACE_FOLDER}";	
    Write-Host "Line $(CurrentLine)   PROJECT_FOLDER                = ${PROJECT_FOLDER}";	
    Write-Host "Line $(CurrentLine)   CORE_FOLDER                   = ${CORE_FOLDER}";	
    Write-Host "Line $(CurrentLine)   PROJECT_NAME                  = ${PROJECT_NAME}";	
    Write-Host "Line $(CurrentLine)   CPU_NAME                      = ${CPU_NAME}";    
    Write-Host "Line $(CurrentLine)   PROJECT_GD32_RELEASE          = ${PROJECT_GD32_RELEASE}";	
    Write-Host "Line $(CurrentLine)   PLATFORM_GD32_NAME            = ${PLATFORM_GD32_NAME}";    
    Write-Host "Line $(CurrentLine)   ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)   ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)   ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)   BOARD_VENDOR_JT               = ${BOARD_VENDOR_JT}";            
    Write-Host "Line $(CurrentLine)   BOARD_VENDOR_JT_LC            = ${BOARD_VENDOR_JT_LC}";            
    Write-Host "Line $(CurrentLine)   BOARD_VENDOR_ST               = ${BOARD_VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)   SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)   ARM_GCC_VERSION               = ${ARM_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)   TEENSY_GCC_VERSION            = ${TEENSY_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)   CMSIS_VERSION                 = ${CMSIS_VERSION}";            
    Write-Host "Line $(CurrentLine)   BREAK_LINE_NO                 = ${BREAK_LINE_NO}";            
    Write-Host "Line $(CurrentLine)   TEMPL_PROJ_NAME               = ${TEMPL_PROJ_NAME}";            
    Write-Host "Line $(CurrentLine)   PROBE                         = ${PROBE}";            
    Write-Host "Line $(CurrentLine)   CPU_TARGET                    = ${CPU_TARGET}";            
    Write-Host "Line $(CurrentLine)   BUILD_SYS                     = ${BUILD_SYS}";            
    Write-Host "Line $(CurrentLine)   PROTOCOL                      = ${PROTOCOL}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_GD32_CFG_FILENAME     = ${OPENOCD_GD32_CFG_FILENAME}";            
    
	Write-Host "Line $(CurrentLine)   PROJECT_GD32_URI              = ${PROJECT_GD32_URI}";
	Write-Host "Line $(CurrentLine)   VARIANTS_GD32_URI             = ${VARIANTS_GD32_URI}";
    Write-Host "Line $(CurrentLine)   CORE_GD32_ZIP_URI             = ${CORE_GD32_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)   PROJECT_GD32_ZIP_NAME         = ${PROJECT_GD32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)   PROJECT_GD32_UNZIPPED_NAME    = ${PROJECT_GD32_UNZIPPED_NAME}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_CORE_GD32_ZIP_NAME    = ${ARDUINO_CORE_GD32_ZIP_NAME}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_CORE_GD32_NAME        = ${ARDUINO_CORE_GD32_NAME}";            
	Write-Host "Line $(CurrentLine)   VARIANTS_GD32_ZIP_NAME        = ${VARIANTS_GD32_ZIP_NAME}";
	Write-Host "Line $(CurrentLine)   VARIANTS_GD32_NAME            = ${VARIANTS_GD32_NAME}";
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)   OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)   XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)   EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_TB_NANE               = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)   OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";

    Write-Host "Line $(CurrentLine)   APP_FOLDER                    = ${APP_FOLDER}";
    Write-Host "Line $(CurrentLine)   APP_SRC_FILENAME              = ${APP_SRC_FILENAME}";
    Write-Host "Line $(CurrentLine)   BUILD_FOLDER                  = ${BUILD_FOLDER}";
    Write-Host "Line $(CurrentLine)   CMAKE_FOLDER                  = ${CMAKE_FOLDER}"
    Write-Host "Line $(CurrentLine)   CMAKE_LIB_FILENAME            = ${CMAKE_LIB_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKE_LIB_MSG_FILENAME        = ${CMAKE_LIB_MSG_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKELISTS_MSG1_FILENAME      = ${CMAKELISTS_MSG1_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKELISTS_MSG2_FILENAME      = ${CMAKELISTS_MSG2_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKE_TOOLCHAIN_FILENAME      = ${CMAKE_TOOLCHAIN_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKE_TOOLCHAIN_msg_FILENAME  = ${CMAKE_TOOLCHAIN_msg_FILENAME}"
    Write-Host "Line $(CurrentLine)   LICENCE_FILENAME              = ${LICENCE_FILENAME}"
    Write-Host "Line $(CurrentLine)   README_FILENAME               = ${README_FILENAME}"
    Write-Host "Line $(CurrentLine)   CMAKELISTS_FILENAME           = ${CMAKELISTS_FILENAME}"

    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)   BASE_PATH                     = ${BASE_PATH}";    
    Write-Host "Line $(CurrentLine)   INSTALL_FOLDER_PATH           = ${INSTALL_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)   DLOAD_PATH                    = ${DLOAD_PATH}";	
    Write-Host "Line $(CurrentLine)   WORKSPACE_FOLDER_PATH         = ${WORKSPACE_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)   PROJECT_FOLDER_PATH           = ${PROJECT_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)   APP_FOLDER_PATH               = ${APP_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)   BUILD_FOLDER_PATH             = ${BUILD_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)   CMAKE_FOLDER_PATH             = ${CMAKE_FOLDER_PATH}";    


    Write-Host "Line $(CurrentLine)   CORE_FOLDER_PATH              = ${CORE_FOLDER_PATH}";	
    Write-Host "Line $(CurrentLine)   WORKSPACE_PATH                = ${WORKSPACE_PATH}";	
    Write-Host "Line $(CurrentLine)   BUILD_FOLDER_PATH             = ${BUILD_FOLDER_PATH}";	
    Write-Host "Line $(CurrentLine)   ARDUINO_PATH                  = ${ARDUINO_PATH}";	
    Write-Host "Line $(CurrentLine)   PORTABLE_PATH                 = ${PORTABLE_PATH}";	
    Write-Host "Line $(CurrentLine)   HARDWARE_PATH_JT              = ${HARDWARE_PATH_JT}";            
    Write-Host "Line $(CurrentLine)   HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)   TOOLS_PATH_JT                 = ${TOOLS_PATH_JT}";            
    Write-Host "Line $(CurrentLine)   TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_PATH                  = ${OPENOCD_PATH}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_CFG_PATH              = ${OPENOCD_CFG_PATH}";            
    Write-Host "Line $(CurrentLine)   PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)   SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)   ARDUINO_CLI_URI               = ${ARDUINO_CLI_URI}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_CFG_SCRIPT_URI        = ${OPENOCD_CFG_SCRIPT_URI}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_LIBRARIES_ZIP_URI     = ${ARDUINO_LIBRARIES_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_ZIP_URI               = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)   SKETCHBOOK_ZIP_URI            = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_TB_NAME               = ${OPENOCD_TB_NAME}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_TB_URI                = ${OPENOCD_TB_URI}";            
    Write-Host "Line $(CurrentLine)   STM32_INDEX                   = ${STM32_INDEX}";            
    Write-Host "Line $(CurrentLine)   KINETIS_INDEX                 = ${KINETIS_INDEX}";            
    Write-Host "Line $(CurrentLine)   STMicroelectronics_INDEX      = ${STMicroelectronics_INDEX}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_LIBRARIES_ZIP_FILE    = ${ARDUINO_LIBRARIES_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   SKETCH_NAME                   = ${SKETCH_NAME}";            
    Write-Host "Line $(CurrentLine)   USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)   PROFILE_PATH                  = ${PROFILE_PATH}";            
    Write-Host "Line $(CurrentLine)   SCRIPT_PATH                   = ${SCRIPT_PATH}";            
    Write-Host "Line $(CurrentLine)   OPENOCD_ZIP_FILE              = ${OPENOCD_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   ARDUINO_CLI_ZIP_FILE          = ${ARDUINO_CLI_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)   SourceFileLocation            = ${SourceFileLocation}";            
    Write-Host "Line $(CurrentLine)   USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)   Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)   Entering                        main"; 

##    Write-Host "Line $(CurrentLine)  Calling                         setup_os_stuff";
##    setup_os_stuff
##Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                          echo_args";
    echo_args                                                                       # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                          create_project_folder";
    create_project_folder
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_app_folder";
    create_app_folder
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_app_sketch";
    create_app_sketch
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_build_folder";
    create_build_folder
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_gdbinit";
    create_gdbinit
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_openocd_config";
    create_openocd_config
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmake_folder";
    create_cmake_folder
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_arduino_lib_cmake";
    create_arduino_lib_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_arduino_lib_msg_cmake";
    create_arduino_lib_msg_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists_msg1_cmake";
    create_cmakelists_msg1_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists_msg2_cmake";
    create_cmakelists_msg2_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_toolchain_cmake";
    create_toolchain_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_toolchain2_cmake";
    create_toolchain2_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_toolchain_msg_cmake";
    create_toolchain_msg_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists_cmake";
    create_cmakelists_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists2_cmake";
    create_cmakelists2_cmake
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists_cmake";
    create_licence
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         create_cmakelists_cmake";
    create_licence
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                         build_project";
    build_project
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# build_project
#---------------------------------------------------------
function build_project
{
    Write-Host "Line $(CurrentLine)  Entering                        build_project";
    Write-Host "Line $(CurrentLine)  Executing                       cd ${BUILD_FOLDER_PATH}";
    cd ${BUILD_FOLDER_PATH}
    Write-Host "Line $(CurrentLine)  PWD                           = ${PWD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Executing                       rm -rf *";
    rm -rf *
    Write-Host "Line $(CurrentLine)  Executing                       cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                       make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                         build_gd32_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# create_cmakelists_cmake
#---------------------------------------------------------
function create_cmakelists_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_cmakelists_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} Contents";

    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'cmake_path(GET CMAKE_CURRENT_SOURCE_DIR FILENAME PROJECT_NAME)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'SET(CCLL CMAKE_CURRENT_LIST_LINE)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'cmake_minimum_required(VERSION      3.22.1)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(CMAKE_VERSION                   3.22.1)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(OPERATING_SYSTEM                Linux)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(OS_LC                           lin)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(DRV                             /home/eicon)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(INSTALL_FOLDER                  J-Tagit)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(ARDUINO_VERSION                 1.8.18)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BUILD_SYSTEM                    cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BOARD_VENDOR                    J-Tagit)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BOARD                           GDDuino)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BOARD_VENDOR_VERSION            1.0.0)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(MCU_PREFIX                      GD32)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(MCU_ARCH                        gd32)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(GD32_CORE_VERSION               1.0.0)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(STM32CUBE_VARIANT               F1_V1.8.3)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(GCC_TOOLCHAIN                   xpack-arm-none-eabi-gcc)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(GCC_VERSION                     10.3.1-2.3)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(GCC_SUFFIX                      "")'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(TOOLS_PATH                      ${DRV}/${INSTALL_FOLDER}/tools)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(DEBUG_PROBE                     jtagit-2)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(PROTOCOL                        swd)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 0 2 MCU_SERIES)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(MCU_SERIES_TYPE                 ${MCU_SERIES}xx)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 0 4 MCU_FAMILY)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 0 8 MCU_PRODUCT)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 4 1 MCU_PIN_COUNT)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 5 1 MCU_FLASH_SIZE)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 6 1 MCU_PACKAGE)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 7 1 MCU_TEMP_RANGE)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(SUBSTRING                    ${PROJECT_NAME} 0 -1 BOARD_SKETCH)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'string(REPLACE "-" ";" PROJECT_NAME_LIST ${PROJECT_NAME})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'list(GET PROJECT_NAME_LIST 0 CPU_NAME)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'list(GET PROJECT_NAME_LIST 1 BOARD_NAME)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'list(GET PROJECT_NAME_LIST 2 SKETCH_NAME)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BUILD_VARIANT_SERIES            ${MCU_PREFIX}${MCU_SERIES}0x)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BUILD_PRODUCT_LINE              ${MCU_PREFIX}${MCU_SERIES}0x_CL)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '##set(BOARD_NAME                      ${STM32_CPU}_${BOARD_SUFFIX})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BASE_PATH                       ${DRV}/${INSTALL_FOLDER})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(STM32_CUBE_PATH                 ${BASE_PATH}/tools/STM32Cube/STM32Cube_FW_${STM32CUBE_VARIANT})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(PROJECTS_PATH                   ${CMAKE_SOURCE_DIR})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(APP_PATH                        ${PROJECTS_PATH}/App)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(CORE_PATH                       ${BASE_PATH}/Core_GD32)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(TOOLCHAIN_PATH                  ${TOOLS_PATH}/xpack-arm-none-eabi-gcc/bin)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(CMSIS_PATH                      ${TOOLS_PATH}/CMSIS)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(VARIANTS_PATH                   ${CORE_PATH}/variants)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(VARIANT_PATH                    ${VARIANTS_PATH}/${BUILD_VARIANT_SERIES}/GD32${MCU_PRODUCT})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(MCU_LINKER_SCRIPT               ${VARIANT_PATH}/ldscript.ld)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(ARDUINO_VERSION2                10818)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(HEX_FILE                        ${PROJECTS_PATH}/build/${PROJECT_NAME}.hex)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(BIN_FILE                        ${PROJECTS_PATH}/build/${PROJECT_NAME}.bin)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(OPENOCD_COMMAND                 ${BASE_PATH}/openocd/bin/openocd)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'set(OPENOCD_CFG                     board/${BOARD_VENDOR}/${OS_LC}/${DEBUG_PROBE}/${PROJECT_NAME}-cmake-${PROTOCOL}.cfg)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/CMakeListsMsg1.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/CMakeListsMsg2.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#return()'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/Toolchain.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/ToolchainMsg.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#return()'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/ArduinoLib.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake/ArduinoLibMsg.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#return()'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'project(${PROJECT_NAME} C CXX ASM)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#return()'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'add_compile_options('
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    -Wall'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    -Wextra'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#    -Wconversion'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '#    -Wsign-conversion'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    -g3'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    -Og'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ')'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'add_library(arduinolib STATIC ${ARDUINOLIB_SOURCES})'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'add_executable(${PROJECT_NAME}.elf ${APP_SOURCES} ${APP_SOURCES2} )'
    Write-Host "Line $(CurrentLine)  Leaving                         create_cmakelists_cmake"
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
}

#---------------------------------------------------------
# create_cmakelists2_cmake
#---------------------------------------------------------
function create_cmakelists2_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_cmakelists2_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} Contents";

    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'target_link_libraries(${PROJECT_NAME}.elf PRIVATE arduinolib)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    COMMENT "Building ${HEX_FILE}' 
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '    Building ${BIN_FILE}"'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ')'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '##add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '##    COMMAND ${OPENOCD_COMMAND} -f ${OPENOCD_CFG}'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '##    COMMENT "Programming ${BOARD}"'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} '##)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'include(CMake\\CMakeListsMsg2.cmake)'
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} ''
    Add-Content ${PROJECT_FOLDER_PATH}${SL}${CMAKELISTS_FILENAME} 'return()'

    Write-Host "Line $(CurrentLine)  Leaving                         create_cmakelists2_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_readme
#---------------------------------------------------------
function create_readme
{
    Write-Host "Line $(CurrentLine)  Entering                        create_readme";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} Contents";
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'SET(CCLL CMAKE_CURRENT_LIST_LINE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}     --------------------------------------------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}     Entering                         Licence.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'TODO Add Licence content'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}    Leaving                          Licence.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${README_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_readme";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_licence
#---------------------------------------------------------
function create_licence
{
    Write-Host "Line $(CurrentLine)  Entering                        create_licence";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} Contents";
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'SET(CCLL CMAKE_CURRENT_LIST_LINE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}     --------------------------------------------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}     Entering                         Licence.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'TODO Add Licence content'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}    Leaving                          Licence.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${LICENCE_FILENAME} 'MESSAGE(STATUS "Licence.cmake  line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_licence";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_toolchain_msg_cmake
#---------------------------------------------------------
function create_toolchain_msg_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_toolchain_msg_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'SET(CCLL CMAKE_CURRENT_LIST_LINE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     --------------------------------------------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     Entering                         ToolChain.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_NAME              = ${CMAKE_SYSTEM_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_VERSION           = ${CMAKE_SYSTEM_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_PROCESSOR         = ${CMAKE_SYSTEM_PROCESSOR}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_COMPILER             = ${CMAKE_CXX_COMPILER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_COMPILER             = ${CMAKE_ASM_COMPILER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJCOPY                  = ${CMAKE_OBJCOPY}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJDUMP                  = ${CMAKE_OBJDUMP}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_AR                       = ${CMAKE_AR}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_TRY_COMPILE_TARG_TYPE    = ${CMAKE_TRY_COMPILE_TARGET_TYPE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    MCU_SERIES_TYPE                = ${MCU_SERIES_TYPE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS                  = ${CORE_FLAGS}  ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS Contd            = --specs=nano.specs --specs=nosys.specs")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS Contd            = -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS1   = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  C_FLAGS       = -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS1   = ${CMAKE_ASM_FLAGS}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  C_FLAGS       = -g -ggdb3 -D__USES_CXX")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    Leaving                          ToolChain.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_MSG_FILENAME} 'MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_toolchain_msg_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_toolchain_cmake
#---------------------------------------------------------
function create_toolchain_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_toolchain_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# the name of the target operating system'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'if (_IS_TOOLCHAIN_PROCESSED)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '    return()'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'endif ()'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(_IS_TOOLCHAIN_PROCESSED True)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_SYSTEM_NAME      Generic)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_SYSTEM_VERSION   1)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_SYSTEM_PROCESSOR ARM)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# which compilers to use for C and C++'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_C_COMPILER   ${TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/arm-none-eabi-g++${GCC_SUFFIX}      CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_ASM_COMPILER ${TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_OBJCOPY      ${TOOLCHAIN_PATH}/arm-none-eabi-objcopy${GCC_SUFFIX}  CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_OBJDUMP      ${TOOLCHAIN_PATH}/arm-none-eabi-objdump${GCC_SUFFIX}  CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_AR           ${TOOLCHAIN_PATH}/arm-none-eabi-ar${GCC_SUFFIX}       CACHE PATH "" FORCE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# core flags'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'if(${MCU_SERIES_TYPE} STREQUAL F3xx)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '	set(CORTEX_FLAGS "-mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'elseif(${MCU_SERIES_TYPE} STREQUAL F1xx)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '	set(CORTEX_FLAGS "-mcpu=cortex-m3 -mfloat-abi=soft")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'else()'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '	MESSAGE(FATAL "CPU Family Series not set ...   see ToolChain.cmake line 35")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'endif()'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(ARM_OPTIONS -mcpu=cortex-m4 -mthumb --specs=nano.specs)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'add_compile_options('
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ${ARM_OPTIONS}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -fmessage-length=0'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -funsigned-char'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -ffunction-sections'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -fdata-sections'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -MMD'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -MP)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'add_compile_definitions('
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ${BUILD_VARIANT_SERIES}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ARDUINO=${ARDUINO_VERSION2}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ARDUINO_${MCU_PRODUCT}_${BOARD_NAME}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ARDUINO_ARCH_${MCU_PREFIX}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  BOARD_NAME=${MCU_PRODUCT}_${BOARD_NAME}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  "VARIANT_H=\"variant_${MCU_PRODUCT}.h\" "'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ${BUILD_PRODUCT_LINE}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#  USE_FULL_ASSERT'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#  OS_USE_TRACE_SEMIHOSTING_STDOUT'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#  OS_USE_SEMIHOSTING'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ')'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'add_link_options('
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  ${ARM_OPTIONS}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#  -fno-use-cxa-atexit'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  --specs=nano.specs'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -specs=nosys.specs'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--defsym=LD_FLASH_OFFSET=0'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--defsym=LD_MAX_SIZE=131072'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--defsym=LD_MAX_DATA_SIZE=20480'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--cref'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--check-sections'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--gc-sections'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--entry=Reset_Handler'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--unresolved-symbols=report-all'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--warn-common'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,--default-script=${MCU_LINKER_SCRIPT}'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -Wl,-Map,${PROJECTS_PATH}/build/${PROJECT_NAME}.map'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -larm_cortexM3l_math'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -L${BASE_PATH}/tools/CMSIS/CMSIS/DSP/Lib/GCC/'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -lc'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -lm'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -lgcc'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '  -lstdc++'   
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#  -nostartfiles'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ')'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''

    Write-Host "Line $(CurrentLine)  Leaving                         create_toolchain_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# create_toolchain2_cmake
#---------------------------------------------------------
function create_toolchain2_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_toolchain2_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '##==========================================================================='
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'if(FALSE) # fake a block comment'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '##==========================================================================='
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# compiler: language specific flags'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CORE_FLAGS "${CORTEX_FLAGS} -mthumb -mthumb-interwork -mlittle-endian --specs=nano.specs --specs=nosys.specs")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_C_FLAGS "${CORE_FLAGS} -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2" CACHE INTERNAL "c compiler flags")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_C_FLAGS_DEBUG        "" CACHE INTERNAL "c compiler flags: Debug")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_C_FLAGS_RELEASE      "" CACHE INTERNAL "c compiler flags: Release")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_CXX_FLAGS            "${CORE_FLAGS} -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3" CACHE INTERNAL "cxx compiler flags")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_CXX_FLAGS_DEBUG      "" CACHE INTERNAL "cxx compiler flags: Debug")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_CXX_FLAGS_RELEASE    "" CACHE INTERNAL "cxx compiler flags: Release")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_ASM_FLAGS            "${CORE_FLAGS} -g -ggdb3 -D__USES_CXX" CACHE INTERNAL "asm compiler flags")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_ASM_FLAGS_DEBUG      "" CACHE INTERNAL "asm compiler flags: Debug")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_ASM_FLAGS_RELEASE    "" CACHE INTERNAL "asm compiler flags: Release")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '#return()'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '##==========================================================================='
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'endif() # fake a block comment'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '##==========================================================================='
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# search for programs in the build host directories'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# for libraries and headers in the target directories'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} '# find additional toolchain executables'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'find_program(ARM_SIZE_EXECUTABLE arm-none-eabi-size)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'find_program(ARM_GDB_EXECUTABLE arm-none-eabi-gdb)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'find_program(ARM_OBJCOPY_EXECUTABLE arm-none-eabi-objcopy)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_TOOLCHAIN_FILENAME} 'find_program(ARM_OBJDUMP_EXECUTABLE arm-none-eabi-objdump)'

    Write-Host "Line $(CurrentLine)  Leaving                         create_toolchain2_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_cmakelists_msg2_cmake
#---------------------------------------------------------
function create_cmakelists_msg2_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_cmakelists_msg2_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 60     -----------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 61     include(CMake/CMakeLitsMsg1.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 62     include(CMake/CMakeListsMsg2.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 63     #return()")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 64     ----")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 65     include(CMake/Toolchain.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 66     include(CMake/ToolchainMsg.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 67     #return()")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 68     ----")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 69     include(CMake/ArduinoLib.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 70     include(CMake/ArduinoLibMsg.cmake)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 71     #return()")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 72     ----")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 73     Executing                        project(${PROJECT_NAME} C CXX ASM)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 74     #return()")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 75     ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 76     add_compile_options( ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 77         -Wall ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 78         -Wextra ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 79     #    -Wconversion")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 80     #    -Wsign-conversion")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 81         -g3 ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 82         -Og ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 83     ) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 84     ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 85     add_library(arduinolib STATIC S{ARDUINOLIB_SOURCES}) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 86     ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 87     add_executable(${PROJECT_NAME}.elf S{APP_SOURCES} S{APP_SOURCES2} ) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 88     ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 89     target_link_libraries(${PROJECT_NAME}.elf PRIVATE arduinolib) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 90     add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 91         COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 92         COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 93         COMMENT \"Building ${HEX_FILE} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 94         Building ${BIN_FILE}\" ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 95        ) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 96     ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 97     ##add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 98     ##    COMMAND ${OPENOCD_COMMAND} -f ${OPENOCD_CFG} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 99     ##    COMMENT \"Programming ${BOARD}\" ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line !00    ##) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 101    ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 102    include(CMake/CMakeListsMain2Message.cmake) ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG2_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line 103     ----------------------------------------------------------------------------------------------------------")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_cmakelists_msg2_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_cmakelists_msg1_cmake
#---------------------------------------------------------
function create_cmakelists_msg1_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_cmakelists_msg1_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     -----------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     PROJECT_NAME                   = ${PROJECT_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     cmake_minimum_required         = VERSION ${CMAKE_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     CMake Version                  = ${CMAKE_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     OPERATING System               = ${OPERATING_SYSTEM}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     OS_LC                          = ${OS_LC}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     Using Hard Drive               = ${DRV}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     INSTALL_FOLDER                 = ${INSTALL_FOLDER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     Arduino Version                = ${ARDUINO_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     BUILD_SYSTEM                  = ${BUILD_SYSTEM}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_Vendor                   = ${BOARD_VENDOR}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD                          = ${BOARD}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_VENDOR_VERSION           = ${BOARD_VENDOR_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PREFIX                     = ${MCU_PREFIX}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_ARCH                       = ${MCU_ARCH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32_CORE_VERSION             = ${STM32_CORE_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32CUBE_VARIANT              = ${STM32CUBE_VARIANT}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_TOOLCHAIN                  = ${GCC_TOOLCHAIN}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_VERSION                    = ${GCC_VERSION}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_SUFFIX                     = ${GCC_SUFFIX}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLS_PATH                     = ${TOOLS_PATH}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    DEBUG_PROBE                    = ${DEBUG_PROBE}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROTOCOL                       = ${PROTOCOL}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_SERIES                     = ${MCU_SERIES}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_SERIES_TYPE                = ${MCU_SERIES_TYPE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_FAMILY                     = ${MCU_FAMILY}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PRODUCT                    = ${MCU_PRODUCT}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PIN_COUNT                  = ${MCU_PIN_COUNT}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_FLASH_SIZE                 = ${MCU_FLASH_SIZE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PACKAGE                    = ${MCU_PACKAGE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_TEMP_RANGE                 = ${MCU_TEMP_RANGE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_NAME                     = ${BOARD_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    SKETCH_NAME                    = ${SKETCH_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECT_NAME                   = ${PROJECT_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECT_NAME_LIST              = ${PROJECT_NAME_LIST}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BUILD_VARIANT_SERIES           = ${BUILD_VARIANT_SERIES}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BUILD_PRODUCT_LINE             = ${BUILD_PRODUCT_LINE}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    --------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_ARCH                       = ${MCU_ARCH}")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BASE_PATH                      = ${BASE_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECTS_PATH                  = ${PROJECTS_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    APP_PATH                       = ${APP_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    CORE_PATH                      = ${CORE_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLS_PATH                     = ${TOOLS_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLCHAIN_PATH                 = ${TOOLCHAIN_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    CMSIS_PATH                     = ${CMSIS_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32_CUBE_PATH                = ${STM32_CUBE_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    VARIANTS_PATH                  = ${VARIANTS_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    VARIANT_PATH                   = ${VARIANT_PATH}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_LINKER_SCRIPT              = ${MCU_LINKER_SCRIPT}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    HEX_FILE                       = ${HEX_FILE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BIN_FILE                       = ${BIN_FILE}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    OPENOCD_COMMAND                = ${OPENOCD_COMMAND}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    OPENOCD_CFG                    = ${OPENOCD_CFG}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKELISTS_MSG1_FILENAME} 'MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_cmakelists_msg1_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_arduino_lib_msg_cmake
#---------------------------------------------------------
function create_arduino_lib_msg_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_arduino_lib_msg_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'SET(CCLL CMAKE_CURRENT_LIST_LINE)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     ----------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     Executing                        add_library(arduinolib STATIC S{ARDUINOLIB_SOURCES})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} '#MESSAGE(STATUS "CMakeLists.txt.cmake  line ${${CCLL}}    ----------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     Including                        CMake/ArduinoLib.cmake}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} '#MESSAGE(STATUS "ToolChain.cmake  line ${${CCLL}}     --------------------------------------------------------------------------------------------------------------------------------------------")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     Processing                     = ArduinoLib.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     MCU_LINKER_SCRIPT              = ${MCU_LINKER_SCRIPT}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}     ARDUINO_VERSION                = ${ARDUINO_VERSION}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    CMAKE_CXX_COMPILER             = ${CMAKE_CXX_COMPILER}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    objcopy                        = ${CMAKE_OBJCOPY}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ---")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    Add CMake Definitions with the following CMake commands")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-D${BUILD_VARIANT_SERIES})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DARDUINO=${ARDUINO_VERSION2})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DARDUINO_${MCU_PRODUCT}_${BOARD_NAME}")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DARDUINO_ARCH_${MCU_PREFIX})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DBOARD_NAME=${MCU_PRODUCT}_${BOARD_NAME})")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DVARIANT_H=\"variant_${MCU_PRODUCT}.h\"")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-D${BUILD_PRODUCT_LINE})")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    add_definitions(-DHAL_UART_MODULE_ENABLED)")' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    End of the Definitions")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ---")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    Add Include Directories with the following CMake commands")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${APP_PATH})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/cores/arduino/api/deprecated)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/cores/arduino/api/deprecated-avr-comp)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/cores/arduino/${MCU_ARCH})")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/startup")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/${BUILD_VARIANT_SERIES}_standard_peripheral/Source)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/${BUILD_VARIANT_SERIES}_standard_peripheral/Include)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}_standard_peripheral/Include)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}/Source/GCC)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}/Source)")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ---")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    Add Source to be compiled with the following CMake commands")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE APP_SOURCES                   ${APP_PATH}/${PROJECT_NAME}.cpp")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE VARIANTS_C_SOURCES            ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.c")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE STARTUP_SOURCES               ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.S")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_C_SOURCES             ${CORE_PATH}/cores/arduino/*.c")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_GD32_C_SOURCES        ${CORE_PATH}/cores/arduino/${MCU_ARCH}/*.c")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_GD32_SOURCE_C_SOURCES ${CORE_PATH}/cores/arduino/${MCU_ARCH}/Source/*.c")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE VARIANTS_CPP_SOURCES          ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.cpp")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_CPP_SOURCES           ${CORE_PATH}/cores/arduino/*.cpp")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_API_CPP_SOURCES       ${CORE_PATH}/cores/arduino/api/*.cpp")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ---")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ---")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    create ARDUINOLIB_SOURCES by combining all of the Library Sources")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ARDUINOLIB_SOURCES             = VARIANTS_C_SOURCES STARTUP_SOURCES ARDUINO_C_SOURCES ARDUINO_GD32_C_SOURCES ARDUINO_GD32_SOURCE_C_SOURCES ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    ARDUINOLIB_SOURCES contd       = ARDUINOLIB_SOURCES VARIANTS_CPP_SOURCES ARDUINO_CPP_SOURCES ARDUINO_API_CPP_SOURCES ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    create APP_SOURCES by combining all of the App Sources and the Linker Script")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    APP_SOURCES                    = ${APP_SOURCES} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    APP_SOURCES2                   = ${APP_SOURCES2} ")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    All done                       = Arduino.cmake")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_MSG_FILENAME} 'MESSAGE(STATUS "ArduinoLib.cmake line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")'

    Write-Host "Line $(CurrentLine)  Leaving                         create_arduino_lib_msg_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_arduino_lib_cmake
#---------------------------------------------------------
function create_arduino_lib_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                        create_arduino_lib_cmake";
    Write-Host "Line $(CurrentLine)  Adding                          ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} Contents";

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'SET(CCLL                                "CMAKE_CURRENT_LIST_LINE")'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '#---  Add Include Directories  ---#'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '#include_directories(${SKETCH_PATH})'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${APP_PATH})'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT})'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/cores/arduino/api/deprecated)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/cores/arduino/api/deprecated-avr-comp)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/cores/arduino/${MCU_ARCH})'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/startup)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/${BUILD_VARIANT_SERIES}_standard_peripheral/Source)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/${BUILD_VARIANT_SERIES}_standard_peripheral/Include)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}_standard_peripheral/Include)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}/Source/GCC)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}_firmware/CMSIS/GD/${BUILD_VARIANT_SERIES}/Source)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '#--- Add Sources  ---#'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE APP_SOURCES                    ${APP_PATH}/${PROJECT_NAME}.cpp)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE VARIANTS_C_SOURCES             ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.c)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE STARTUP_SOURCES                ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.S)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE ARDUINO_C_SOURCES              ${CORE_PATH}/cores/arduino/*.c)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE ARDUINO_GD32_C_SOURCES         ${CORE_PATH}/cores/arduino/${MCU_ARCH}/*.c)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE ARDUINO_GD32_SOURCE_C_SOURCES  ${CORE_PATH}/cores/arduino/${MCU_ARCH}/Source/*.c)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE VARIANTS_CPP_SOURCES           ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.cpp)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE ARDUINO_CPP_SOURCES            ${CORE_PATH}/cores/arduino/*.cpp)'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'file(GLOB_RECURSE ARDUINO_API_CPP_SOURCES        ${CORE_PATH}/cores/arduino/api/*.cpp)'

    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'set(ARDUINOLIB_SOURCES   ${VARIANTS_C_SOURCES} ${STARTUP_SOURCES} ${ARDUINO_C_SOURCES} ${ARDUINO_GD32_C_SOURCES} ${ARDUINO_GD32_SOURCE_C_SOURCES} )'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} 'set(ARDUINOLIB_SOURCES   ${ARDUINOLIB_SOURCES} ${VARIANTS_CPP_SOURCES} ${ARDUINO_CPP_SOURCES} ${ARDUINO_API_CPP_SOURCES} )' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} ''
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '#set(APP_SOURCES     ${SKETCH_CPP_SOURCES} ${MCU_LINKER_SCRIPT} )'
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '' 
    Add-Content ${CMAKE_FOLDER_PATH}${SL}${CMAKE_LIB_FILENAME} '#---------------  All Done  ---------------------------------------------------------------------------------------------------#'

    Write-Host "Line $(CurrentLine)  Leaving                         create_arduino_lib_cmake";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_cmake_folder
#---------------------------------------------------------
function create_cmake_folder
{
    Write-Host "Line $(CurrentLine)  Entering                        create_cmake_folder";
    Write-Host "Line $(CurrentLine)  Creating                        ${PROJECT_FOLDER_PATH}${SL}${CMAKE_FOLDER}";
    Write-Host "Line $(CurrentLine)  Executing                       md -p ${PROJECT_FOLDER_PATH}${SL}${CMAKE_FOLDER}";

    md -p ${PROJECT_FOLDER_PATH}${SL}${CMAKE_FOLDER}

    Write-Host "Line $(CurrentLine)  Leaving                         create_cmake_folder";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_config
#---------------------------------------------------------
function create_openocd_config
{
    Write-Host "Line $(CurrentLine)  Entering                        create_openocd_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File                exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File                does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} "# ${BOARD_VENDOR} ${PROJECT_NAME}"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_JT}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} "source [find target/${BOARD_VENDOR_JT}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} "program ${BUILD_GD32_PATH}/${PROJECT_NAME}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                         ${OPENOCD_CFG_PATH}/${OPENOCD_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_openocd_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_gdbinit
#---------------------------------------------------------
function create_gdbinit
{
    Write-Host "Line $(CurrentLine)  Entering                        create_gdbinit";
    Write-Host "Line $(CurrentLine)  Adding                          ${DRIVE}/.gdbinit Contents";
    Add-Content $DRIVE/.gdbinit "set auto-load safe-path ${BUILD_STM32_PATH}/.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                          ${BUILD_FOLDER_PATH}/.gdbinit Contents";
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "set history save on"
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "set pagination off"
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "set print pretty on"
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "set confirm off"
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_FOLDER_PATH}/.gdbinit "b ${APP_FOLDER_PATH}/${APP_SRC_FILENAME}:${BREAK_LINE_NO}"
    Write-Host "Line $(CurrentLine)  Editing                         .gdbinit Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_gdbinit";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_build_folder
#---------------------------------------------------------
function create_build_folder
{
    Write-Host "Line $(CurrentLine)  Entering                        create_build_folder";
    Write-Host "Line $(CurrentLine)  Creating                        ${PROJECT_FOLDER_PATH}${SL}${BUILD_FOLDER}";
    Write-Host "Line $(CurrentLine)  Executing                       md -p ${PROJECT_FOLDER_PATH}${SL}${BUILD_FOLDER}";

    md -p ${PROJECT_FOLDER_PATH}${SL}${BUILD_FOLDER}

    Write-Host "Line $(CurrentLine)  Leaving                         create_build_folder";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_app_sketch
#---------------------------------------------------------
function create_app_sketch
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering                        create_app_sketch";
    Write-Host "Line $(CurrentLine)  Adding                          ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME}.cpp Contents";

    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '/*'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  Turns an LED on for one second, then off for one second, repeatedly.'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  Performs a Serial.println("Worldd \r\n");'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  This example code is in the public domain.'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '*/'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '#include <Arduino.h>'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} ''
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '// the setup function runs once when you press reset or power the board'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} 'void setup() {'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  // initialize digital pin LED_BUILTIN as an output.'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  pinMode(LED_BUILTIN, OUTPUT);'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  Serial.begin(9600);'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '}'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} ''
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '// the loop function runs over and over again forever'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} 'void loop() {'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  Serial.println("Hello");           // Test Serial print line'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  delay(1000);                       // wait for a second'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  Serial.println("Worldd \r\n");     // Test Serial print line'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '  delay(1000);                       // wait for a second'
    Add-Content ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME} '}'

    Write-Host "Line $(CurrentLine)  Editing                         ${APP_FOLDER_PATH}${SL}${APP_SRC_FILENAME}.cpp Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_app_sketch";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# create_app_folder
#---------------------------------------------------------
function create_app_folder
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering                        create_app_folder";

    Write-Host "Line $(CurrentLine)  Creating                        ${PROJECT_FOLDER_PATH}${SL}${APP_FOLDER}";
    Write-Host "Line $(CurrentLine)  Executing                       md -p ${PROJECT_FOLDER_PATH}${SL}${APP_FOLDER}";
    md -p ${PROJECT_FOLDER_PATH}${SL}${APP_FOLDER}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Leaving                         create_app_folder";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_project_folder
#---------------------------------------------------------
function create_project_folder
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering                        create_project_folder";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${PROJECT_FOLDER_PATH})";
    if (Test-Path ${PROJECT_FOLDER_PATH})
    {
        Write-Host "Line $(CurrentLine)  Project Folder Path             Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${PROJECT_FOLDER_PATH}";
        Remove-Item -Recurse -Force ${PROJECT_FOLDER_PATH}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project Folder Path             does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Creating                        ${PROJECT_FOLDER_PATH}";
    Write-Host "Line $(CurrentLine)  Executing                       md -p ${PROJECT_FOLDER_PATH}";
    md -p ${PROJECT_FOLDER_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                         create_project_folder";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                         main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#---------------------------------------------------------
# build_gd32_project
#---------------------------------------------------------
function build_gd32_project
{
    Write-Host "Line $(CurrentLine)  Entering                        build_gd32_project";
    cd ${BUILD_GD32_PATH}
    Write-Host "Line $(CurrentLine)  PWD                           = ${PWD}";
    Write-Host "Line $(CurrentLine)  Executing                       rm -rf *";
    rm -rf *
    Write-Host "Line $(CurrentLine)  Executing                       cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                       make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                         build_gd32_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# build_stm32_project
#---------------------------------------------------------
function build_stm32_project
{
    Write-Host "Line $(CurrentLine)  Entering                        build_stm32_project";
    cd ${BUILD_STM32_PATH}
    Write-Host "Line $(CurrentLine)  PWD                           = ${PWD}";
    Write-Host "Line $(CurrentLine)  Executing                       rm -rf *";
    rm -rf *
    Write-Host "Line $(CurrentLine)  Executing                       cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                       make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                         build_stm32_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_gd32_config
#---------------------------------------------------------
function create_openocd_gd32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        create_openocd_gd32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File                exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File                does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "# ${BOARD_VENDOR_JT} ${PROJECT_NAME_GD}"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_JT}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "source [find target/${BOARD_VENDOR_JT}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "program ${BUILD_GD32_PATH}/${PROJECT_NAME_ST}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                         ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_openocd_gd32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_stm32_config
#---------------------------------------------------------
function create_openocd_stm32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        create_openocd_stm32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File                exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File                does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "# ${BOARD_VENDOR_JT} ${PROJECT_NAME_ST}"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_JT}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "source [find target/${BOARD_VENDOR_JT}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "program ${BUILD_STM32_PATH}/${PROJECT_NAME_ST}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                         ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_openocd_stm32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# rename_files
#---------------------------------------------------------
function rename_files
{
    Write-Host "Line $(CurrentLine)  Entering                        rename_files";
    Write-Host "Line $(CurrentLine)  Executing                       Rename-Item  ${APP_PATH}/${TEMPL_PROJ_NAME}.cpp  ${APP_PATH}/${PROJECT_NAME_ST}.cpp";
    Rename-Item  ${APP_PATH}/${TEMPL_PROJ_NAME}.cpp  ${APP_PATH}/${PROJECT_NAME_ST}.cpp

    Write-Host "Line $(CurrentLine)  Leaving                         rename_files";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_codelite_gd32_config
#---------------------------------------------------------
function install_codelite_gd32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        install_codelite_gd32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DRIVE}/.gdbinit)";
    if (Test-Path ${DRIVE}/.gdbinit)    
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        exists  ...  continuing" ; 
#        Remove-Item -Recurse -Force ${DRIVE}/.gdbinit
    }
	else
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${DRIVE}/.gdbinit Contents";
    Add-Content $DRIVE/.gdbinit "set auto-load safe-path ${BUILD_STM32_PATH}/.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                          ${BUILD_GD32_PATH}/.gdbinit Contents";
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set history save on"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set pagination off"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set print pretty on"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set confirm off"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "b ${APP_PATH}/${PROJECT_NAME_GD}.cpp:${BREAK_LINE_NO}"
    Write-Host "Line $(CurrentLine)  Editing                         .gdbinit Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         install_codelite_gd32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_codelite_stm32_config
#---------------------------------------------------------
function install_codelite_stm32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        install_codelite_stm32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DRIVE}/.gdbinit)";
    if (Test-Path ${DRIVE}/.gdbinit)    
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${DRIVE}/.gdbinit
    }
	else
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${DRIVE}/.gdbinit Contents";
    Add-Content $DRIVE/.gdbinit "set auto-load safe-path ${BUILD_STM32_PATH}/.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                          ${BUILD_STM32_PATH}/.gdbinit Contents";
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set history save on"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set pagination off"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set print pretty on"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set confirm off"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "b ${APP_PATH}/${PROJECT_NAME_ST}.cpp:${BREAK_LINE_NO}"
    Write-Host "Line $(CurrentLine)  Editing                         .gdbinit Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         install_codelite_stm32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# iinstall_j-tagit_gd32_variants-lin
#---------------------------------------------------------
function iinstall_j-tagit_gd32_variants-lin
{
    Write-Host "Line $(CurrentLine)  Entering                        iinstall_j-tagit_gd32_variants-lin";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                        exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                        do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      does not exist  ...  continuing" ;  
    }

    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                    exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_NAME})";
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped               exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${VARIANTS_GD32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_GD32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Executing                       Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_GD32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_GD32_NAME}";
    Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_GD32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_GD32_NAME}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Leaving                         iinstall_j-tagit_gd32_variants-lin";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_j-tagit_stm32_variants-lin
#---------------------------------------------------------
function install_j-tagit_stm32_variants-lin
{
    Write-Host "Line $(CurrentLine)  Entering                        install_j-tagit_stm32_variants-lin";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                        exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                        do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      does not exist  ...  continuing" ;  
    }

    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                    exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP+NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_NAME})";
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped               exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${VARIANTS_STM32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_STM32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Executing                       Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_STM32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_STM32_NAME}";
    Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_STM32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_STM32_NAME}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Leaving                         install_j-tagit_stm32_variants-lin";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_gd32_cmake_project
#---------------------------------------------------------
function install_arduino_gd32_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_gd32_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD})
    {
        Write-Host "Line $(CurrentLine)  Project                         Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                         does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                      ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${PROJECT_GD32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_GD32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                        ${WORKSPACE_PATH}/${PROJECT_GD32_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${WORKSPACE_PATH}/${PROJECT_GD32_UNZIPPED_NAME}  ${WORKSPACE_PATH}/${PROJECT_NAME_GD}

    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_gd32_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_stm32_cmake_project
#---------------------------------------------------------
function install_arduino_stm32_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_stm32_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST})
    {
        Write-Host "Line $(CurrentLine)  Project                         Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                         does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                      ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${PROJECT_STM32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_STM32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                        ${WORKSPACE_PATH}/${PROJECT_STM32_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${WORKSPACE_PATH}/${PROJECT_STM32_UNZIPPED_NAME}  ${WORKSPACE_PATH}/${PROJECT_NAME_ST}

    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_stm32_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_gd32_core
#---------------------------------------------------------
function install_arduino_gd32_core
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_gd32_core";


    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${CORE_GD32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${CORE_GD32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${CORE_GD32_PATH})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${CORE_GD32_PATH})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                   is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${CORE_GD32_PATH}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${CORE_GD32_PATH}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                        ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_GD32_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${CORE_GD32_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_GD32_NAME}  ${CORE_GD32_PATH}
		
    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_gd32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_arduino_stm32_core
#---------------------------------------------------------
function install_arduino_stm32_core
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_stm32_core";


    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${CORE_STM32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${CORE_STM32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${CORE_STM32_PATH})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${CORE_STM32_PATH})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                   is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${CORE_STM32_PATH}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${CORE_STM32_PATH}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                        ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_STM32_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${CORE_STM32_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_STM32_NAME}  ${CORE_STM32_PATH}
		
    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_stm32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

